# [Section] Listts 
names = ["John", "Paul", "George", "Ringo"]
programs = ['Developer', "pi-shape", "Short course"]
durations = [260, 180, 20]
truth_values = [True, False, True, False]

# For getting the length of the list 
print(len(programs))

# For getting an item from the list 
print(programs[0])

# for getting items based on an index range
# the first index must be lower than the second one
print(programs[0:2])

#you can update the calue of a specifit item in the list by calling the item by the index and assigning a new value to it
print(programs[2])
programs[2] = 'Short Courses'
print(programs[2])

# [Section] List Manipulation 

## Add a new item into the list
durations.append(367)
print(durations)

# Deleting an item from the list
del durations [-1]
print(durations)

# For sorting a list. The sort() method sorts the list by ascendign order by default
names.sort()
print(names)

# for clearing/emptying the list
test_list = [1, 3, 5, 7, 9,]
test_list.clear()
print(test_list)

# [Section] Dictionaries
person1 = {
    "name" : "Elon",
    "age" : 70,
    "occupation" : "Student",
    "isEnrolled" : True,
    "Subjects" : ["Python", "SQL", "Django"]
}

print(person1)

# getting the length of the dictionary 
print(len(person1))

# getting a specific value based on the key 
print(person1["name"])

# getting all the keys in the dictionary
print(person1.keys())

# getting all the values in the dictionary
print(person1.values())

# Getting all the items along with both their keys and values
print(person1.items())

# [Section] Dictionary Manipulation 
# adding a key-value pair to a dictionary can be done using the 'update()' function
person1["nationality"] = "Norwegian"
person1.update({'fave_food' : "dinuguan"})
print(person1)

person1.pop("fave_food")
del person1['nationality']
print(person1)

# for clearing all of the entries in the dictionary
person2 = {
    "name" : "Mystika",
    "age" : 18
}

person2.clear()
print(person2)

#nested dictionaries
person3 = {
    "name" : "monika",
    "age" : 21,
    "occupation" : "Lyricist",
    "isEnrolled" : True,
    "Subjects" : ["Python", "SQL", " Ruby"]
}

classRoom = {
    "student1" : person1,
    "student2" : person3
}

print(classRoom)

# [Function]
def my_greeting():
    print("Hello user!")

my_greeting()

def greet_user(username):
    print(f"hello, {username}")

greet_user("Elon")

# Lambda Functions are anonumous functions that can execute shrot code expressions
greeting = lambda person : f"hello {person}!"
print(greeting("else"))
print(greeting("tony"))

# [Section] CLasses 

class Car():
    def __init__(self, brand, model, year_of_make): 
        self.brand = brand
        self.model = model
        self.year_of_make = year_of_make

        #other properties
     
        self.fuel : "Gasoline"
        self.fuel_level = 0
        self.distance_driven = 0

        
    
    def fill_fuel(self):
        print(f"Current fuel level : {self.fuel_level}")
        print(f"...filling up the fuel tank")
        self.fuel_level = 100
        print(f"New fuel level : {self.fuel_level}")

    #MINI ACTIVITY
    # Declare a drive function for the car class which will prin the total distance that the car has already driven 
    # Along with that, also print the fuel level minus the distance that the car has driven. 

    def drive(self, distance=0):

        print(f"Total distance driven: {self.distance_driven}")
        self.distance_driven += distance
        print(f"New distance driven: {self.distance_driven}")
        self.fuel_level -= distance
        print(f"Fuel level after driving {distance}km: {self.fuel_level}")

                

new_car = Car("Maserati", "Explorer", "2005")
print(f"My car is a {new_car.brand} {new_car.model}")
new_car.fill_fuel()
new_car.drive(50)


