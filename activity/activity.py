
class Camper:
    def __init__(self, name, batch, course_type):
        self.name = name
        self.batch = batch
        self.course_type = course_type
        
    def info(self):
            print(f"My name is {self.name} of batch {self.batch}")

    def career_track(self):
        print(f"{self.name} is currently enrolled in the {self.course_type} program.")
    
    

new_camper = Camper("Jherson", "b123", "Python")
print(f"Camper Name : {new_camper.name}")
print(f"Camper Batch : {new_camper.batch}")
print(f"Camper Course : {new_camper.course_type}")
new_camper.info()
new_camper.career_track()

